# SoberTools

#### 介绍
SoberTools是一个`dotnet tools`项目，采用`C#`编写并发布到`https://www.nuget.org/`平台供安装，像`NodeJS`中的`npm`一样。

#### 记录小点

1.  使用第三包，详情地址:`https://dotnetchina.gitee.io/furion/docs/dotnet-tools/`
2.  全局安装包
    ```csharp
    dotnet tool install --global --add-source ./nupkg HelloTools
    ```
3.  全局更新包
    ```csharp
    dotnet tool update  --global --add-source ./nupkg HelloTools
    ```
4.  全局卸载包
    ```csharp
    dotnet tool uninstall --global HelloTools
    ```   

#### 使用说明

1.  见帮助


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
