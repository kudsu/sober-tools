﻿using Furion.Tools.CommandLine;
using RestSharp;
using System;
using System.Collections.Generic;

namespace SoberTools
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Hello World!");
        //}
        // 通过 Cli.Inject() 完成准备工作
        static void Main(string[] args) => Cli.Inject();
        #region 基础
        /// <summary>
        /// 输出 Hello 名字
        /// </summary>
        [Argument('n', "name", "您的名字")]
        static string Name { get; set; }
        // 定义参数处理程序，必须 [属性名]+Handler
        static void NameHandler(ArgumentMetadata argument)
        {
            Console.WriteLine($"hahaha,Hello {Name}");
        }

        /// <summary>
        /// 查看版本
        /// </summary>
        [Argument('v', "version", "工具版本号")]
        static bool Version { get; set; }
        // 定义参数处理程序，必须 [属性名]+Handler
        static void VersionHandler(ArgumentMetadata argument)
        {
            Console.WriteLine(Cli.GetVersion());
        }
        /// <summary>
        /// 桌面路径
        /// </summary>
        [Argument('a', "address", "获取Windows特殊目录")]
        static string Address { get; set; }
        // 定义参数处理程序，必须 [属性名]+Handler
        static void AddressHandler(ArgumentMetadata argument)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "desktop", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) },
                { "username", Environment.UserName },
                { "machinename", Environment.MachineName },
                { "hosts", @"C:\Windows\System32\drivers\etc" }
            };
            if (string.IsNullOrEmpty(Address))
            {
                Cli.WriteLine("结果如下：", ConsoleColor.Blue);
                foreach (var item in dic)
                {
                    Cli.WriteLine(item.Key + "：" + item.Value, ConsoleColor.Blue);
                }

                Cli.WriteLine($"可指定数据库\n请尝试输入：sober -d sqlite\n得到结果：Data Source=./Furion.db", ConsoleColor.Red);   // 字体颜色
                return;
            }


            if (dic.ContainsKey(Address))
            {
                Cli.WriteLine($"结果如下：{dic[Address]}", ConsoleColor.Blue);
            }
            else
            {
                Console.WriteLine($"未收录【{Address}】信息");
            }
        }

        /// <summary>
        /// 查看帮助文档
        /// </summary>
        [Argument('h', "help", "查看帮助文档")]
        static bool Help { get; set; }
        // 定义参数处理程序，必须 [属性名]+Handler
        static void HelpHandler(ArgumentMetadata argument)
        {
            Cli.GetHelpText("hello-tools");
        }

        // 所有未匹配的参数/操作符处理程序，固定 NoMatchesHandler 方法名
        static void NoMatchesHandler(bool isEmpty, string[] operands, Dictionary<string, object> noMatches)
        {
            if (isEmpty)
            {
                Console.WriteLine(@"
   ____        __             ______             __     
  / __/ ___   / /  ___   ____/_  __/ ___  ___   / /  ___
 _\ \  / _ \ / _ \/ -_) / __/ / /   / _ \/ _ \ / /  (_-<
/___/  \___//_.__/\__/ /_/   /_/    \___/\___//_/  /___/
");
                Console.WriteLine($"欢迎使用{Cli.GetDescription()}");
            }
            if (operands.Length > 0) Cli.Error($"未找到该操作符：{string.Join(",", operands)}");
            if (noMatches.Count > 0) Cli.Error($"未找到该参数：{string.Join(",", noMatches.Keys)}");
        }
        #endregion
        [Argument('r', "random", "随机")]
        static bool Random { get; set; }
        static void RandomHandler(ArgumentMetadata argument)
        {
            var client = new RestClient("https://api.uomg.com/api/rand.qinghua?format=text");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Cookie", "PHPSESSID=u5s7fodbfnddbe9br2pbtt5v24");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
        }

        [Argument('d', "getdb", "获取数据库连接字符串")]
        static string DatabaseConnectionString { get; set; }
        static void DatabaseConnectionStringHandler(ArgumentMetadata argument)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "sqlite", "Data Source=./Furion.db" },
                { "mysql", "Data Source=localhost;Database=Furion;User ID=root;Password=000000;pooling=true;port=3306;sslmode=none;CharSet=utf8;" },
                { "sqlserver", "Server=localhost;Database=Furion;User=sa;Password=000000;MultipleActiveResultSets=True;" },
                { "oracle", "User Id=orcl;Password=orcl;Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=orcl)))" },
                { "postgresql", "PORT=5432;DATABASE=postgres;HOST=127.0.0.1;PASSWORD=postgres;USER ID=postgres;" }
            };
            if (string.IsNullOrEmpty(DatabaseConnectionString))
            {
                Cli.WriteLine("结果如下：", ConsoleColor.Blue);
                foreach (var item in dic)
                {
                    Cli.WriteLine(item.Key + "：" + item.Value, ConsoleColor.Blue);
                }

                Cli.WriteLine($"可指定数据库\n请尝试输入：sober -d sqlite\n得到结果：Data Source=./Furion.db", ConsoleColor.Red);   // 字体颜色
                return;
            }


            if (dic.ContainsKey(DatabaseConnectionString))
            {
                Cli.WriteLine($"结果如下：{dic[DatabaseConnectionString]}", ConsoleColor.Blue);
            }
            else
            {
                Console.WriteLine($"未收录【{DatabaseConnectionString}】数据库的字符串");
            }
        }
    }
}
